package com.lim.jetty;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created by Alexander Pampushko on 12.09.2016.
 */
public class RedirectServlet extends HttpServlet
{
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
	{
		String location = req.getParameter("page");

		if (location != null)
		{
			if (location.equals("googel"))
			{
				resp.sendRedirect("http://www.google.com");
			}
			else if (location.equals("welcome"))
			{
				resp.sendRedirect("index.jsp");
			}
		}

		resp.setContentType("text/html");

		PrintWriter writer = resp.getWriter();

		writer.println("<!DOCTYPE html>");
		writer.println("<head>");
		writer.println("<meta charset=\"utf-8\">");
		writer.println("<title>Invalid page</title>");
		writer.println("</head>");

		writer.println("<body>");
		writer.println("<header>");
		writer.println("<hgroup>");
		writer.println("<h1>");
		writer.println("Invalid page requested <br />");
		writer.println("<p><a href='redirectServlet.html'</p>");
		writer.println("Welcome to Servlets!");
		writer.println("</h1>");
		writer.println("</hgroup>");
		writer.println("</header>");
		writer.println("</body>");
		writer.println("</html>");
		writer.close(); //для завершения страницы закрыть поток

	}
}
