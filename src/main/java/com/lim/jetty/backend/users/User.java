package com.lim.jetty.backend.users;

/**
 * Created by Limmy on 21.09.2016.
 */
public class User {
    private static long id = 0;
    private String userName;
    private String password;
    private String email;
    UserType type;

    public User() { // FOR TESTS ONLY
        userName = "unnamed-" + id++;
        password = "";
        type = UserType.GUEST;
    }


    public User(String userName, String password, String email) {
        id++;
        this.userName = userName;
        this.password = password;
        this.email = email;
        type = UserType.STANDART_USER;
    }

    public String getUserName() {
        return userName;
    }

    public String getPassword() {
        return password;
    }

    public String getEmail() {
        return email;
    }

    public UserType getType() {
        return type;
    }

    public boolean promoteUserToModerator() {
        if (this.type != UserType.STANDART_USER) return false;
        else {
            this.type = UserType.MODERATOR;
            UsersDBHandler.getInstance().updateUser(this.userName, this);
        }
        return true;
    }

    public boolean demoteUserFromModerator() {
        if (this.type != UserType.MODERATOR) return false;
        else {
            this.type = UserType.STANDART_USER;
            UsersDBHandler.getInstance().updateUser(this.userName, this);
        }
        return true;
    }

    public boolean giveUserVIPStatus() {
        if (this.type != UserType.STANDART_USER) return false;
        else {
            this.type = UserType.VIP_USER;
            UsersDBHandler.getInstance().updateUser(this.userName, this);
        }
        return true;
    }

    public boolean removeUserVIPStatus() {
        if (this.type != UserType.VIP_USER) return false;
        else {
            this.type = UserType.STANDART_USER;
            UsersDBHandler.getInstance().updateUser(this.userName, this);
        }
        return true;
    }

    public boolean removeUser() {
        return UsersDBHandler.getInstance().deleteUser(this.userName);
    }

    @Override
    public String toString() {
        return "User:" +
                "userName='" + userName + '\'' +
                ", password='" + password + '\'' +
                ", email='" + email + '\'' +
                ", type=" + type +
                '}';
    }
}
