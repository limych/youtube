package com.lim.jetty.backend.users;

import com.mysql.fabric.jdbc.FabricMySQLDriver;

import java.io.Closeable;
import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Limmy on 21.09.2016.
 */
public class UsersDBHandler implements Closeable {
    private static UsersDBHandler instance = null;
    private UsersDBHandler() {}
    public static synchronized UsersDBHandler getInstance() {
        if (instance == null) instance = new UsersDBHandler();

        return instance;
    }
    private final String DATABASE_URL = "jdbc:mysql://localhost:3306/mytube";
    private final String DATABASE_USERNAME = "root";
    private final String DATABASE_PASSWORD = "root";
    private Connection connection;
    private synchronized void openConnection() throws SQLException{
        Driver driver = new FabricMySQLDriver();
        DriverManager.registerDriver(driver);
        connection = DriverManager.getConnection(DATABASE_URL, DATABASE_USERNAME, DATABASE_PASSWORD);
    }
    public synchronized void createUser(User user) {
        try {
            if (connection == null || connection.isClosed()) openConnection();
            Statement statement = connection.createStatement();
            statement.execute("INSERT INTO USERS VALUES ('" + user.getUserName() +"','" + user.getPassword() + "','" + user.getEmail() +
                    "','"+ user.getType().toString() + "','2012-06-18 10:34:09')");
        } catch (SQLException e) {
            System.err.println("Fail to create and write user " + user.getUserName());
            e.printStackTrace();
        }
    }
    public synchronized List<User> readAllUsers() {
        List<User> allUsers = new ArrayList<>();
        try {
            if (connection == null || connection.isClosed()) openConnection();
            Statement statement = connection.createStatement();
            ResultSet set = statement.executeQuery("SELECT * FROM USERS");
            while (set.next()) {
                User newUser = new User(set.getString(1), set.getString(2), set.getString(3));
                allUsers.add(newUser);
            }
        }
        catch (SQLException e) {
            e.printStackTrace();
        }

        return allUsers;
    }

    public synchronized User readUserInfo(String name) {
        try {
            if (connection == null || connection.isClosed()) openConnection();
            Statement statement = connection.createStatement();
            ResultSet count = statement.executeQuery("SELECT count(*) FROM USERS WHERE username = '" + name + "'");
            int size = 0;
            while (count.next()) {
                size++;
            }
            if (size == 0) {
                statement.close();
                return null;
            }
            else {
                User currentUser = null;
                ResultSet set = statement.executeQuery("SELECT * FROM USERS WHERE username = '" + name + "'");
                while (set.next()) {
                    currentUser = new User(set.getString(1), set.getString(2), set.getString(3));
                    break;
                }
                statement.close();
                return currentUser;
            }

        }
        catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }


    public synchronized void updateUser(String name, User newUserInfo) {
        try {
            if (connection == null || connection.isClosed()) openConnection();
            Statement statement = connection.createStatement();
            statement.execute("UPDATE users SET password = '"
                    + newUserInfo.getPassword() +
                    "', email = '" + newUserInfo.getEmail() + "', usertype = '"
                    + newUserInfo.getType().toString() + "'" +
                    "WHERE username = '" + name + "';");
            statement.close();
        }
        catch (SQLException e) {
            System.err.println("Не удалось обновить пользователя " + name);
            e.printStackTrace();
        }
    }
    public synchronized boolean deleteUser(String name) {
        try {
            if (connection == null || connection.isClosed()) openConnection();
            Statement statement = connection.createStatement();
            boolean isSucces = statement.execute("DELETE FROM USERS WHERE username = '" + name + "'");
            statement.close();
            return isSucces;
        }
        catch (SQLException e) {
            System.err.println("Не удалось удалить пользователя " + name);
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public void close() throws IOException {
        try {
            if (connection != null) connection.close();
        } catch (SQLException e) {
            System.err.println("Не удалось закрыть соединение с базой данных Users");
            e.printStackTrace();
        }
    }
}
