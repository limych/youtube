package com.lim.jetty.backend.users;

/**
 * Created by Limmy on 21.09.2016.
 */
public enum UserType {
    GUEST,
    STANDART_USER,
    VIP_USER,
    MODERATOR,
    ADMINISTATOR
}
