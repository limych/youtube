package com.lim.jetty;

/**
 * Created by Alexander Pampushko on 05.09.2016.
 */


import org.eclipse.jetty.websocket.api.Session;
import org.eclipse.jetty.websocket.api.annotations.*;
import org.eclipse.jetty.websocket.servlet.WebSocketServlet;
import org.eclipse.jetty.websocket.servlet.WebSocketServletFactory;

import java.io.IOException;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

@SuppressWarnings("serial")
@WebSocket
public class HelloWebSocketServlet extends WebSocketServlet
{

	private Session session;

	private ScheduledExecutorService executor = Executors.newScheduledThreadPool(1);

	// called when the socket connection with the browser is established
	@OnWebSocketConnect
	public void handleConnect(Session session) {
		this.session = session;
	}

	// called when the connection closed
	@OnWebSocketClose
	public void handleClose(int statusCode, String reason) {
		System.out.println("Connection closed with statusCode="
				+ statusCode + ", reason=" + reason);
	}

	// called when a message received from the browser
	@OnWebSocketMessage
	public void handleMessage(String message) {
		if ("start".equals(message)) {
			send("Stock service started!");
			final Runnable runnable = new Runnable() {
				public void run() {
					send(TestService.getStockInfo());
				}
			};
			executor.scheduleAtFixedRate(runnable, 0, 5, TimeUnit.SECONDS);
		} else {
			this.stop();
		}
	}

	// called in case of an error
	@OnWebSocketError
	public void handleError(Throwable error) {
		error.printStackTrace();
	}

	// sends message to browser
	private void send(String message) {
		try {
			if (session.isOpen()) {
				session.getRemote().sendString(message);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	// closes the socket
	private void stop()
	{
		try
		{
			session.disconnect();
		} catch (IOException e)
		{
			e.printStackTrace();
		}
	}



	@Override
	public void configure(WebSocketServletFactory factory)
	{
		// set a 10 second timeout
		factory.getPolicy().setIdleTimeout(10000);

		// register MyEchoSocket as the WebSocket to create on Upgrade
		factory.register(HelloWebSocketServlet.class);
	}
}
