package com.lim.jetty.frontend;

import javax.servlet.ServletException;
import javax.servlet.SingleThreadModel;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by Alexander Pampushko on 16.09.2016.
 */
public class LoginServlet extends HttpServlet implements SingleThreadModel
{
	@Override
	public String getServletInfo()
	{
		return super.getServletInfo();
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
	{

	}
	
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
	{
		req.getRequestDispatcher("index.jsp").forward(req, resp);
	}
}
