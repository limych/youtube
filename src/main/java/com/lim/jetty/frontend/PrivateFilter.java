package com.lim.jetty.frontend;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * Created by Alexander Pampushko on 18.09.2016.
 */
public class PrivateFilter implements Filter
{
	@Override
	public void init(FilterConfig filterConfig) throws ServletException
	{
		
	}
	
	@Override
	public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException
	{
//		//преобразуем запрос и ответ
//		HttpServletRequest httpServletRequest = (HttpServletRequest) servletRequest;
//		HttpServletResponse httpServletResponse = (HttpServletResponse) servletResponse;
//
//		//получаем сессию
//		HttpSession session = httpServletRequest.getSession();
//		//если сессия не создана, перенаправляем на страницу логина
//		if (session == null)
//		{
//			httpServletResponse.sendRedirect("/login.jsp");
//		}
//
//		//получаем атрибут сессии "user"
//		User user = (User) session.getAttribute("user");
//		//если юзер определен и этот юзер залогинился, то отправляем запрос на запрашиваему страницу
//		if (user != null && user.isLogged())
//		{
//			filterChain.doFilter(servletRequest, servletResponse);
//		}
//		else //если юзер не заполнен или не прошел логин, то перенаправляем на страницу логина
//		{
//			httpServletResponse.sendRedirect("/login.jsp");
//		}
	}
	
	@Override
	public void destroy()
	{
		
	}
	
	
}
