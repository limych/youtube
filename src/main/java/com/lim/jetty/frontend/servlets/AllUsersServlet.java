package com.lim.jetty.frontend.servlets;

import com.lim.jetty.backend.users.User;
import com.lim.jetty.backend.users.UsersDBHandler;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created by Limmy on 27.09.2016.
 */
@WebServlet(name = "welcome")
public class AllUsersServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");
        PrintWriter writer = response.getWriter();
        StringBuilder builder = new StringBuilder();
        builder.append("<!DOCTYPE html><head><title>AllUsersHere</title></head>\n<body>");
        builder.append("\n<h2> ALL USERS: </h2><ul>");
        for (User us: UsersDBHandler.getInstance().readAllUsers()) {
            builder.append("<li>" + us.getUserName() + "</li>\n");
        }
        builder.append("</ul></body>\n</html>");
        writer.write(builder.toString());
        writer.flush();
        writer.close();
    }
}
