<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html lang="ru">
<head>
	<title>Вход на сайт</title>
	<meta http-equiv="Content-Type" content="text/html;charset=UTF8">
	<meta http-equiv="X-UA-Compatible" content="IE=EDGE">
	<meta http-equiv="refresh" content="20">
	<link rel="stylesheet" href="//aui-cdn.atlassian.com/aui-adg/6.0.0/css/aui.min.css" media="all">
	<link rel="stylesheet" href="//aui-cdn.atlassian.com/aui-adg/6.0.0/css/aui-experimental.min.css" media="all">
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
	<script src="//aui-cdn.atlassian.com/aui-adg/6.0.0/js/aui.min.js"></script>
	<script src="//aui-cdn.atlassian.com/aui-adg/6.0.0/js/aui-experimental.min.js"></script>
	<script src="//aui-cdn.atlassian.com/aui-adg/6.0.0/js/aui-datepicker.min.js"></script>
	<script src="//aui-cdn.atlassian.com/aui-adg/6.0.0/js/aui-soy.min.js"></script>

</head>
<body class="aui-page-focused aui-page-size-large"><!-- For a normal page, omit all 'aui-page-' classes here -->

<div id="page">
	<header id="header" role="banner">
		<nav class="aui-header aui-dropdown2-trigger-group" role="navigation">
			<div class="aui-header-inner">
				<div class="aui-header-primary">
					<ul class="aui-nav">
						<li><a href="http://example.com/">Загрузить видео</a></li>
						<li><a href="/profile.jsp">Профиль</a></li>
						<li><a href="/logout.jsp">Выйти</a></li>
					</ul>
				</div>
			</div>
		</nav>
	</header>
	<section id="content" role="main">
		<header class="aui-page-header">
			<div class="aui-page-header-inner">
				<div class="aui-page-header-main">
					<h1>Вход в систему</h1>
				</div>
			</div>
		</header>

		<div class="aui-page-panel">
			<div class="aui-page-panel-inner">
				<section class="aui-page-panel-content">

					<form class="aui" action="login" method="post">
						<div class="field-group">
							<label for="email-input">Email
								<span class="aui-icon icon-required">(required)</span></label>
							<input class="text medium-long-field" type="text"
							       id="email-input" name="login" placeholder="you@example.com">
							<div class="description">Your email address.</div>
						</div>
						<div class="field-group">
							<label for="password-input">Password</label>
							<input class="text medium-long-field" type="password" name="password"
							          id="password-input" placeholder="Введите свой пароль...">
						</div>
						<div class="buttons-container">
							<div class="buttons">
								<input class="button submit" type="submit" value="Login" id="do-login-button">
							</div>
						</div>
					</form>

				</section>
			</div>
		</div>
	</section>

</div>
</body>
</html>