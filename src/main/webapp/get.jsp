<%--
  Created by IntelliJ IDEA.
  User: algernon
  Date: 09.09.2016
  Time: 15:43
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="ru">
<head>
	<meta http-equiv="Content-Type" content="text/html;charset=UTF8">
	<title>Обработка запросов "GET" с данными</title>
</head>
<body>
<%
	String name = request.getParameter("firstName");
	if (name != null)
	{


%>
<h1>
	Hello, <%= name%><br />
	Welcome to JavaServer Pages!
</h1>
<%
	}
	else
	{
%>

<form action="get.jsp" method="get">
	<p>Введите свое имя и нажмите кнопку "Послать"</p>
	<p>
		<input type="text" name="firstName" />
		<input type="submit" value="Отправить" />
	</p>

</form>
<%
	} //конец else
%>
<%-- конец скриптлета --%>
</body>
</html>
